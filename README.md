#Kanban

An extremely basic kanban.

###Prior Art

This is based off of the meteor/todos app using the meteor guide.

This is a Todos example app built on the principles described in the [Meteor Guide](http://guide.meteor.com/structure.html). This app uses the module functionality introduced in Meteor 1.3.

This branch (`react`) implements the UI in [React](https://facebook.github.io/react/index.html). You can read more about using React in Meteor in the [Meteor guide article on the subject](http://guide.meteor.com/v1.3/react.html).

### Running the app

```bash
cd app
```

```bash
meteor npm install
meteor
```

### Scripts

To lint:

```bash
meteor npm run lint
```
