import { Meteor } from 'meteor/meteor';
import { Lists } from '../../api/lists/lists.js';
import { Cards } from '../../api/cards/cards.js';

// if the database is empty on server start, create some sample data.
Meteor.startup(() => {
  if (Lists.find().count() === 0) {
    const data = [
      {
        name: 'Ice Box',
        items: [
          'W-9 from Zak',
          'Work Agreement with Zak',
        ],
      },
      {
        name: 'In Progress',
        items: [
          'Terms with Zak',
        ],
      },
      {
        name: 'Testing',
        items: [
          'Zak/s Meteor Prowess',
        ],
      },
      {
        name: 'Ready for Release',
        items: [
        ],
      },
      {
        name: 'Complete',
        items: [
          "Email interview with Zak"
        ],
      },
    ];

    let timestamp = (new Date()).getTime();

    data.forEach((list) => {
      const listId = Lists.insert({
        name: list.name,
        incompleteCount: list.items.length,
      });

      list.items.forEach((text) => {
        Cards.insert({
          listId,
          text,
          createdAt: new Date(timestamp),
        });

        timestamp += 1; // ensure unique timestamp.
      });
    });
  }
});
