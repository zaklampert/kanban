import React from 'react';
import update from 'react/lib/update';
import { DropTarget } from 'react-dnd';
import AddCard from './AddCard';
import Card from './Card.jsx';
import { move } from '../../api/cards/methods';

const listTarget = {
  drop(props, monitor) {
    const card = monitor.getItem();

    if (props.listObj._id !== card.listId) {
      return move.call({ cardId: card.id, listId: props.listObj._id });
    }
    // const cardIndex = card.originalIndex;
    // TODO: handle reorder.
    return undefined;
  },
};

function collect(connect, monitor) {
  return {
    // Call this function inside render()
    // to let React DnD handle the drag events:
    connectDropTarget: connect.dropTarget(),
    // You can ask the monitor about the current drag state:
    isOver: monitor.isOver(),
    item: monitor.getItem(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
    itemType: monitor.getItemType(),
  };
}

class List extends React.Component {
  constructor(props) {
    super(props);
    this._moveCard = this._moveCard.bind(this);
    this._findCard = this._findCard.bind(this);
    this.state = {
      cards: [],
      receivingDrop: false,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.isOver && nextProps.item.listId !== this.props.listObj._id) {
      return this.setState({
        receivingDrop: true,
      });
    }
    return this.setState({
      cards: nextProps.cards,
      receivingDrop: false,
    });
  }
  _moveCard(id, atIndex) {
    const { card, index } = this._findCard(id);
    this.setState(update(this.state, {
      cards: {
        $splice: [
          [index, 1],
          [atIndex, 0, card],
        ],
      },
    }));
  }

  _findCard(id) {
    const { cards } = this.state;
    if (!cards[0]) {
      return false;
    }
    const card = cards.filter(c => c._id === id)[0];
    return {
      card,
      index: cards.indexOf(card),
    };
  }
  render() {
    const { list, connectDropTarget } = this.props;
    const { cards, receivingDrop } = this.state;
    return connectDropTarget(
      <div className="list">
        <h2>{list.name}</h2>
        {(receivingDrop) ? <div
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
            background: 'yellow',
          }}
        /> : null }
        {cards.map((card, i) => {
          if (card && card._id) {
            return (
              <Card
                key={card._id}
                id={card._id}
                tracking={card.tracking}
                listId={card.listId}
                text={card.text}
                index={i}
                moveCard={this._moveCard}
                findCard={this._findCard}
              />
            );
          }
          return null;
        })}

        <AddCard listId={list._id} />
      </div>
    );
  }
}

List.propTypes = {
  listObj: React.PropTypes.object,
  list: React.PropTypes.object,
  connectDropTarget: React.PropTypes.func,
};

export default DropTarget('CARD', listTarget, collect)(List);
