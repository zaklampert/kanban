import React from 'react';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import ListContainer from '../containers/ListContainer.jsx';

class Kanban extends React.Component {
  render() {
    const { lists } = this.props;
    return (
      <div id="kanban">
        {lists.map(list => (
          <ListContainer key={list._id} listObj={list} />
        ))}
      </div>
    );
  }
}

Kanban.propTypes = ({
  lists: React.PropTypes.array,
});

export default DragDropContext(HTML5Backend)(Kanban);
