import React, { PropTypes } from 'react';
import { DragSource, DropTarget } from 'react-dnd';

import { updateText, remove, createLabel } from '../../api/cards/methods';

const style = {
  border: '1px dashed gray',
  padding: '0.5rem 1rem',
  marginBottom: '.5rem',
  backgroundColor: 'white',
  cursor: 'move',
};

const cardSource = {
  beginDrag(props) {
    return {
      id: props.id,
      text: props.text,
      listId: props.listId,
      originalIndex: props.findCard(props.id).index,
    };
  },

  endDrag(props, monitor) {
    const { id: droppedId, originalIndex } = monitor.getItem();
    const didDrop = monitor.didDrop();
    if (didDrop) {
      props.moveCard(droppedId, originalIndex);
    }
  },
};

const cardTarget = {
  canDrop() {
    return false;
  },
  hover(props, monitor) {
    const { id: draggedId, listId: cardListId } = monitor.getItem();
    const { id: overId, listId: overListId } = props;
    if (cardListId !== overListId) {
      return false;
    }
    if (draggedId !== overId) {
      const { index: overIndex } = props.findCard(overId);
      return props.moveCard(draggedId, overIndex);
    }
    return undefined;
  },
};

/**
 * Specifies the props to inject into your component.
 */
function collectDrag(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
  };
}

function collectDrop(connect) {
  return {
    connectDropTarget: connect.dropTarget(),
  };
}

const propTypes = {
  text: PropTypes.string.isRequired,
  id: PropTypes.string,
  // moveCard: PropTypes.func.isRequired,
  // Injected by React DnD:
  tracking: PropTypes.string,
  isDragging: PropTypes.bool.isRequired,
  connectDragSource: PropTypes.func.isRequired,
  connectDropTarget: PropTypes.func,
};

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editing: false,
      newText: props.text,
      labeling: false,
    };
    this._toggleEdit = this._toggleEdit.bind(this);
    this._renderCard = this._renderCard.bind(this);
    this._renderEdit = this._renderEdit.bind(this);
    this._handleEditSubmit = this._handleEditSubmit.bind(this);
    this._toggleCreateLabelForm = this._toggleCreateLabelForm.bind(this);
  }

  _toggleEdit() {
    this.setState({
      editing: !this.state.editing,
    });
  }
  _toggleCreateLabelForm() {
    this.setState({
      labeling: !this.state.labeling,
    });
  }
  // 'ship_to.company_name': { type: String },
  // 'ship_to.address.address_line_1': { type: String },
  // 'ship_to.address.city': { type: String },
  // 'ship_to.address.state_code': { type: String },
  // 'ship_to.address.country_code': { type: String },
  // 'ship_to.address.postal_code': { type: String },
  _renderCard() {
    const { text, id, tracking } = this.props;
    const { labeling } = this.state;
    if (labeling) {
      return (
        <div>
          <form
            onSubmit={
              (e) => {
                e.preventDefault();
                if (this.company_name.value &&
                    this.address_line_1.value &&
                    this.city.value &&
                    this.state_code.value &&
                    this.country_code.value &&
                    this.postal_code.value) {
                  createLabel.call({
                    cardId: id,
                    ship_to: {
                      company_name: this.company_name.value,
                      address: {
                        address_line_1: this.address_line_1.value,
                        city: this.city.value,
                        state_code: this.state_code.value,
                        country_code: this.country_code.value,
                        postal_code: this.postal_code.value,
                      },
                    },
                  });
                  return this.setState({
                    labeling: false
                  })
                }
              }
            }>
            <input name="company_name" ref={(c) => { this.company_name = c; }} placeholder="Company Name"/>
            <input name="address_line_1" ref={(c) => { this.address_line_1 = c; }} placeholder="Address" />
            <input name="city" ref={(c) => { this.city = c; }} placeholder="City" />
            <input name="state_code" ref={(c) => { this.state_code = c; }} placeholder="State Code" />
            <input name="country_code" ref={(c) => { this.country_code = c; }} placeholder="Country Code" />
            <input name="postal_code" ref={(c) => { this.postal_code = c; }} placeholder="Postal Code" />
            <button type="submit">Submit</button>
          </form>
        </div>
      );
    }
    return (
      <div>
        {text}
        {(tracking) ? <img src={`/${tracking}.gif`} style={{maxWidth: '100%'}} /> :null }
        <div className="controls">
          <div className="control" onClick={this._toggleEdit}>Edit</div>
           <div className="control" onClick={this._toggleCreateLabelForm}>Ship </div>
          <div className="control" onClick={() => remove.call({ cardId: id })}>Delete</div>
        </div>
      </div>
    );
  }
  _handleEditSubmit(e) {
    if (e && e.preventDefault) {
      e.preventDefault();
    }
    const { newText } = this.state;
    const { id } = this.props;
    if (newText !== '') {
      updateText.call({ cardId: id, newText });
      this.setState({
        editing: false,
      });
    }
  }
  _renderEdit() {
    return (
      <div>
        <form onSubmit={this._handleEditSubmit}>
          <input
            onChange={e => this.setState({ newText: e.target.value })}
            value={this.state.newText}
          />
          <button type="submit">Update</button>
        </form>

        <div onClick={this._toggleEdit}>Cancel</div>
      </div>
    );
  }

  render() {
    const { isDragging, connectDragSource, connectDropTarget } = this.props;
    const opacity = isDragging ? 0 : 1;
    const { editing } = this.state;
    return connectDragSource(
      connectDropTarget(
        <div style={{ ...style, opacity }}>
          {(editing) ? this._renderEdit() : this._renderCard()}
        </div>
      )
    );
  }
}

Card.propTypes = propTypes;

// Export the wrapped component:
export default DragSource('CARD', cardSource, collectDrag)(DropTarget('CARD', cardTarget, collectDrop)(Card));
