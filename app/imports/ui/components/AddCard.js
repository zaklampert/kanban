import React from 'react';
import { insert } from '../../api/cards/methods';

class AddCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addingCard: false,
      cardText: '',
    };
    this._toggleAddForm = this._toggleAddForm.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
    this._handleTextChange = this._handleTextChange.bind(this);
  }
  _toggleAddForm() {
    this.setState({
      addingCard: !this.state.addingCard,
    });
  }
  _handleTextChange(event) {
    this.setState({
      cardText: event.target.value,
    });
  }
  _renderAddCardForm() {
    return (
      <div>
        <form onSubmit={this._handleSubmit}>
          <input
            onChange={this._handleTextChange}
            value={this.state.cardText}
          />
          <button type="submit">Add</button>
        </form>
        <div onClick={this._toggleAddForm}>
          cancel
        </div>
      </div>
    );
  }
  _handleSubmit(e) {
    if (e && e.preventDefault) {
      e.preventDefault();
    }
    const { cardText } = this.state;
    const { listId } = this.props;
    if (cardText !== '') {
      // TODO: handle insert errors
      insert.call({ listId, text: cardText });
      return this.setState({
        cardText: '',
        addingCard: false,
      });
    }
    return false;
  }
  render() {
    const { addingCard } = this.state;
    if (addingCard) {
      return this._renderAddCardForm();
    }
    return (
      <div onClick={this._toggleAddForm}>
         + Add Card
      </div>
    );
  }
}

AddCard.propTypes = {
  listId: React.PropTypes.string,
};

export default AddCard;
