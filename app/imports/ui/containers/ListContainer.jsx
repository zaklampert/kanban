import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Lists } from '../../api/lists/lists.js';
import Component from '../components/List.jsx';

const ListContainer = createContainer(({listObj}) => {
  const cardsHandle = Meteor.subscribe('cards.inList', { listId: listObj._id });
  const loading = !cardsHandle.ready();
  const list = Lists.findOne(listObj._id);
  const listExists = !loading && !!list;
  return {
    loading,
    list,
    listExists,
    cards: listExists ? list.cards().fetch() : [],
  };
}, Component);

export default ListContainer;
