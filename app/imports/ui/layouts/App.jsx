import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session'; // XXX: SESSION
import { Lists } from '../../api/lists/lists.js';


import ConnectionNotification from '../components/ConnectionNotification.jsx';
import Kanban from '../components/Kanban.jsx';

const CONNECTION_ISSUE_TIMEOUT = 5000;

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
      showConnectionIssue: false,
    };
    this.toggleMenu = this.toggleMenu.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      /* eslint-disable react/no-did-mount-set-state */
      this.setState({ showConnectionIssue: true });
    }, CONNECTION_ISSUE_TIMEOUT);
  }

  // componentWillReceiveProps({ loading, children }) {
  //   // redirect / to a list once lists are ready
  //   if (!loading && !children) {
  //     const list = Lists.findOne();
  //     this.context.router.replace(`/lists/${list._id}`);
  //   }
  // }

  toggleMenu(menuOpen = !Session.get('menuOpen')) {
    Session.set({ menuOpen });
  }

  logout() {
    Meteor.logout();

    // if we are on a private list, we'll need to go to a public one
    if (this.props.params.id) {
      const list = Lists.findOne(this.props.params.id);
      if (list.userId) {
        const publicList = Lists.findOne({ userId: { $exists: false } });
        this.context.router.push(`/lists/${publicList._id}`);
      }
    }
  }

  render() {
    const { showConnectionIssue } = this.state;
    const {
      user,
      connected,
      lists,
      menuOpen,
    } = this.props;

    return (
      <div id="container" className={menuOpen ? 'menu-open' : ''}>
        {/* <UserMenu user={user} logout={this.logout} /> */}
        {showConnectionIssue && !connected
          ? <ConnectionNotification />
          : null}

        <Kanban
          user={user}
          lists={lists}
        />
      </div>
    );
  }
}

App.propTypes = {
  user: React.PropTypes.object,      // current meteor user
  connected: React.PropTypes.bool,   // server connection status
  // loading: React.PropTypes.bool,     // subscription status
  menuOpen: React.PropTypes.bool,    // is side menu open?
  lists: React.PropTypes.array,      // all lists visible to the current user
  // children: React.PropTypes.element, // matched child route component
  // location: React.PropTypes.object,  // current router location
  params: React.PropTypes.object,    // parameters of the current route
};

App.contextTypes = {
  router: React.PropTypes.object,
};
