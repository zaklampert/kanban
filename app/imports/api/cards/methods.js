import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';

import { Cards } from './cards.js';
import { Lists } from '../lists/lists.js';

export const insert = new ValidatedMethod({
  name: 'cards.insert',
  validate: new SimpleSchema({
    listId: { type: String },
    text: { type: String },
  }).validator(),
  run({ listId, text }) {
    const list = Lists.findOne(listId);

    if (list.isPrivate() && list.userId !== this.userId) {
      throw new Meteor.Error('api.todos.insert.accessDenied',
        'Cannot add todos to a private list that is not yours');
    }

    const card = {
      listId,
      text,
      createdAt: new Date(),
    };

    Cards.insert(card);
  },
});



//TODO: We need to have a methods to reorder the cards
//and move them to a different list

// export const setCheckedStatus = new ValidatedMethod({
//   name: 'todos.makeChecked',
//   validate: new SimpleSchema({
//     todoId: { type: String },
//     newCheckedStatus: { type: Boolean },
//   }).validator(),
//   run({ todoId, newCheckedStatus }) {
//     const todo = Todos.findOne(todoId);
//
//     if (todo.checked === newCheckedStatus) {
//       // The status is already what we want, let's not do any extra work
//       return;
//     }
//
//     if (!todo.editableBy(this.userId)) {
//       throw new Meteor.Error('api.todos.setCheckedStatus.accessDenied',
//         'Cannot edit checked status in a private list that is not yours');
//     }
//
//     Todos.update(todoId, { $set: {
//       checked: newCheckedStatus,
//     } });
//   },
// });

export const move = new ValidatedMethod({
  name: 'cards.move',
  validate: new SimpleSchema({
    cardId: { type: String },
    listId: { type: String },
  }).validator(),
  run({ cardId, listId }) {
    // This is complex auth stuff - perhaps denormalizing a userId onto todos
    // would be correct here?
    const card = Cards.findOne(cardId);

    if (!card.editableBy(this.userId)) {
      throw new Meteor.Error('api.todos.updateText.accessDenied',
        'Cannot edit todos in a private list that is not yours');
    }

    Cards.update(cardId, {
      $set: { listId: listId },
    });
  },
});

export const updateText = new ValidatedMethod({
  name: 'cards.updateText',
  validate: new SimpleSchema({
    cardId: { type: String },
    newText: { type: String },
  }).validator(),
  run({ cardId, newText }) {
    // This is complex auth stuff - perhaps denormalizing a userId onto todos
    // would be correct here?
    const card = Cards.findOne(cardId);

    if (!card.editableBy(this.userId)) {
      throw new Meteor.Error('api.todos.updateText.accessDenied',
        'Cannot edit todos in a private list that is not yours');
    }

    Cards.update(cardId, {
      $set: { text: newText },
    });
  },
});

export const createLabel = new ValidatedMethod({
  name: 'cards.createShippingLabel',
  validate: new SimpleSchema({
    cardId: { type: String },
    'ship_to.company_name': { type: String },
    'ship_to.address.address_line_1': { type: String },
    'ship_to.address.city': { type: String },
    'ship_to.address.state_code': { type: String },
    'ship_to.address.country_code': { type: String },
    'ship_to.address.postal_code': { type: String },
  }).validator(),
  run({ cardId, ship_to }) {
    console.log('Creating Shipping Label For ' + cardId);
    if (Meteor.isServer) {
      const upsApi = require('./server/ups-api');

      upsApi.createShippingLabel({
        shipper: {
          name: 'Sostena',
          shipper_number: '27546F',
          address: {
            address_line_1: '123 Fake Address',
            city: 'Sacremento',
            state_code: 'CA',
            country_code: 'US',
            postal_code: '94023',
          },
        },
        ship_to,
        packages: [
          {
            description: 'My Package',
            weight: 10,
          },
        ],
      }).then((res) => {
        Cards.update(cardId, {
          $set: { shipped: true, tracking: res.tracking }
        })


      }).catch(err => {
        console.log(err);

      });
    }
  },
});

export const remove = new ValidatedMethod({
  name: 'cards.remove',
  validate: new SimpleSchema({
    cardId: { type: String },
  }).validator(),
  run({ cardId }) {
    const card = Cards.findOne(cardId);

    if (!card.editableBy(this.userId)) {
      throw new Meteor.Error('api.todos.remove.accessDenied',
        'Cannot remove todos in a private list that is not yours');
    }

    Cards.remove(cardId);
  },
});

// Get list of all method names on Todos
const CARDS_METHODS = _.pluck([
  insert,
  // setCheckedStatus,
  updateText,
  move,
  remove,
], 'name');

if (Meteor.isServer) {
  // Only allow 5 todos operations per connection per second
  DDPRateLimiter.addRule({
    name(name) {
      return _.contains(CARDS_METHODS, name);
    },

    // Rate limit per connection ID
    connectionId() { return true; },
  }, 5, 1000);
}
