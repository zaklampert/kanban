import { Mongo } from 'meteor/mongo';
import { Factory } from 'meteor/factory';
import faker from 'faker';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

// import incompleteCountDenormalizer from './incompleteCountDenormalizer.js';
import { Lists } from '../lists/lists.js';

class CardsCollection extends Mongo.Collection {
  insert(doc, callback) {
    const ourDoc = doc;
    ourDoc.createdAt = ourDoc.createdAt || new Date();
    const result = super.insert(ourDoc, callback);
    // incompleteCountDenormalizer.afterInsertTodo(ourDoc);
    return result;
  }
  update(selector, modifier) {
    const result = super.update(selector, modifier);
    // incompleteCountDenormalizer.afterUpdateTodo(selector, modifier);
    return result;
  }
  remove(selector) {
    const card = this.find(selector).fetch();
    const result = super.remove(selector);
    // incompleteCountDenormalizer.afterRemoveTodos(todos);
    return result;
  }
}

export const Cards = new CardsCollection('cards');

// Deny all client-side updates since we will be using methods to manage this collection
Cards.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Cards.schema = new SimpleSchema({
  listId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    // denyUpdate: true,
  },
  text: {
    type: String,
    max: 100,
  },
  createdAt: {
    type: Date,
    denyUpdate: true,
  },
  checked: {
    type: Boolean,
    defaultValue: false,
  },
  shipped: {
    type: Boolean,
    defaultValue: false,
  },
  tracking: {
    type: String,
  },
});

Cards.attachSchema(Cards.schema);

// This represents the keys from Lists objects that should be published
// to the client. If we add secret properties to List objects, don't list
// them here to keep them private to the server.
Cards.publicFields = {
  listId: 1,
  text: 1,
  createdAt: 1,
  order: 1,
  tracking: 1,
};


Factory.define('card', Cards, {
  listId: () => Factory.get('list'),
  text: () => faker.lorem.sentence(),
  createdAt: () => new Date(),
});

Cards.helpers({
  list() {
    return Lists.findOne(this.listId);
  },
  editableBy(userId) {
    return this.list().editableBy(userId);
  },
});
