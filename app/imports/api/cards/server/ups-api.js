import fs from 'fs';
import util from 'util';
import UPS from 'shipping-ups';
import { Meteor } from 'meteor/meteor';

if (Meteor.isServer) {
  const ups = new UPS({
    environment: Meteor.settings.ups.environment, // or live
    access_key: Meteor.settings.ups.access_key,
    username: Meteor.settings.ups.username,
    password: Meteor.settings.ups.password,
  });

  export const timeInTransit = ({ from, to }) => {

    ups.time_in_transit({
      from,
      to,
      // from: {
      //   city: 'Sacramento',
      //   state_code: 'CA',
      //   postal_code: '94203',
      //   country_code: 'US'
      // },
      // to: {
      //   city: 'Springfield',
      //   state_code: 'MO',
      //   postal_code: '65804',
      //   country_code: 'US'
      // }
    }, function( err, res ) {
      if (err) {
        console.log(err);
      }

      return (util.inspect(res, { depth: null }));
    });
  };

  export const createShippingLabel = ({ shipper, ship_to, packages }) => {
    return new Promise(( resolve, reject ) => {
      ups.confirm({
        shipper,
        ship_to,
        packages,

      }, {
        transaction_id: 'ABC123',
        // extra_params: {
        //   Shipment: {
        //     ShipmentServiceOptions: {
        //       Notification: {
        //         NotificationCode: '2',
        //         EMailMessage: {
        //           EMailAddress: 'hello@myemailaddress.com',
        //           UndeliverableEMailAddress: 'noreply@myemailaddress.com',
        //           FromEMailAddress: 'from@myemailaddress.com',
        //           Memo: 'We thought you might like to know',
        //           Subject: 'Your package has shipped from our store',
        //         },
        //       },
        //     },
        //   },
        // },
      }, function (err, res) {
        if (err) {
          return reject(err);
        }



        // return console.log(util.inspect(res, {depth: null}));

        ups.accept(res.ShipmentDigest, function(err, res) {
          if(err) {
            return reject(err);
          }
          const tracking = res.ShipmentResults.PackageResults.TrackingNumber;

          // NOTE: probably should just throw this up on aws or something instead (zl)
          const path = process.env['METEOR_SHELL_DIR'] + '/../../../public';

          fs.writeFile( path +'/' + tracking + '.gif', new Buffer(res.ShipmentResults.PackageResults.LabelImage.GraphicImage, "base64"), function(err) {

            resolve({
              tracking,
              success: true,
            })
            // ups.void(res.ShipmentResults.ShipmentIdentificationNumber, function(err, res) {
            //   // {shipment_identification_number: '1Z648616E192760718'}
            //   if(err) {
            //     return console.log(err);
            //   }
            //
            //   console.log(util.inspect(res, {depth: null}));
            // })
          });
        });
      });
    });

  }
  //
  // ups.address_validation({
  //   name: 'Customer Name',
  //   address_line_1: '123 Test Address',
  //   city: 'Charlotte',
  //   state_code: 'NC',
  //   postal_code: '28205',
  //   country_code: 'US'
  // }, function(err, res) {
  //   if(err) {
  //     console.log(err);
  //   }
  //
  //   console.log(util.inspect(res, {depth: null}));
  // });
  //
  // ups.rates({
  //   shipper: {
  //     name: 'Type Foo',
  //     address: {
  //       address_line_1: '123 Fake Address',
  //       city: 'Dover',
  //       state_code: 'OH',
  //       country_code: 'US',
  //       postal_code: '44622'
  //     }
  //   },
  //   ship_to: {
  //     name: 'Uhsem Blee',
  //     address: {
  //       address_line_1: '3456 Fake Address',
  //       city: 'Charlotte',
  //       state_code: 'NC',
  //       country_code: 'US',
  //       postal_code: '28205'
  //     }
  //   },
  //   packages: [
  //     {
  //       description: 'My Package',
  //       weight: 10
  //     }
  //   ]
  // }, function(err, res) {
  //   if(err) {
  //     return console.log(err);
  //   }
  //   console.log(util.inspect(res, {depth: null}));
  //   // should return an array of rates
  // });
  //
  // ups.track('1ZY291F40369744809', function(err, res) {
  //   if(err) {
  //     return console.log(err);
  //   }
  //
  //   console.log(util.inspect(res, {depth: null}));
  // });
  //
  // ups.confirm({
  //   shipper: {
  //     name: 'Type Foo',
  //     shipper_number: 'R419W8',
  //     address: {
  //       address_line_1: '123 Fake Address',
  //       city: 'Dover',
  //       state_code: 'OH',
  //       country_code: 'US',
  //       postal_code: '44622'
  //     }
  //   },
  //   ship_to: {
  //     company_name: 'Uhsem Blee',
  //     address: {
  //       address_line_1: '3456 Fake Address',
  //       city: 'Charlotte',
  //       state_code: 'NC',
  //       country_code: 'US',
  //       postal_code: '28205'
  //     }
  //   },
  //   packages: [
  //     {
  //       description: 'My Package',
  //       weight: 10
  //     }
  //   ]
  // }, {transaction_id: 'ABC123', extra_params: {Shipment: {ShipmentServiceOptions: {Notification: {
  //   NotificationCode: '2',
  //   EMailMessage: {
  //     EMailAddress: 'hello@myemailaddress.com',
  //     UndeliverableEMailAddress: 'noreply@myemailaddress.com',
  //     FromEMailAddress: 'from@myemailaddress.com',
  //     Memo: 'We thought you might like to know',
  //     Subject: 'Your package has shipped from our store'
  //   }
  // }}}}}, function(err, res) {
  //   if(err) {
  //     return console.log(err);
  //   }
  //
  //   //console.log(util.inspect(res, {depth: null}));
  //   ups.accept(res.ShipmentDigest, function(err, res) {
  //     if(err) {
  //       return console.log(err);
  //     }
  //
  //     fs.writeFile('./label.gif', new Buffer(res.ShipmentResults.PackageResults.LabelImage.GraphicImage, "base64"), function(err) {
  //       ups.void(res.ShipmentResults.ShipmentIdentificationNumber, function(err, res) {
  //         // {shipment_identification_number: '1Z648616E192760718'}
  //         if(err) {
  //           return console.log(err);
  //         }
  //
  //         console.log(util.inspect(res, {depth: null}));
  //       })
  //     });
  //   });
  // });

}
